# Authorship Network Singularity

## Overview

This repository contains the Singularity definition for building a container containing several network building tools for creating an authorship network figure.

The definition file builds from rocker/verse and contains RStudio server.

## Building

The container is built via CI/CD automatically when 'Singularity.def' is modified. 

## Pulling

The resulting sif file is only pushed to the registry when a tag is used for a commit. The latest version of the built container can be pulled from the registry using:

```
IMAGE_DIR="/work/$USER/images"
mkdir -p $IMAGE_DIR
apptainer pull --force --dir $IMAGE_DIR oras://gitlab-registry.oit.duke.edu/chart-consortium/containers/authorship-network-singularity/authorship-network-singularity:latest
```

## Running

The primary method of running the container is Open OnDemand via the RStudio Singularity commmunity app. 
